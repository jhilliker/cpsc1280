
___path_prune__uniq() {
	exec awk '
		BEGIN { RS=":" ; FS="" ; ORS=":" }
		$0 != "" && arr[$0] != 1 {
			arr[$0] = 1;
			print;
		}
	'
}

___path_prune__ensureExists() {
	local dir
	while IFS= read -rd ':' dir ; do
		[ -d "$dir" ] && printf '%s:' "$dir"
	done
}

___path_prune__colons() {
	# removes duplicate ':', leading, and trailing
	exec sed -e 's/::*/:/g' -e 's/^://' -e 's/:$//'
}

___path_prune__PATH="$(
	printf '%s' "$PATH" \
	| ___path_prune__uniq \
	| ___path_prune__ensureExists \
	| ___path_prune__colons
)"
[ -n "$___path_prune__PATH" ] \
	&& PATH="$___path_prune__PATH" \
	&& unset $(set | grep -o '___path_prune__\w*' | sort -u)
