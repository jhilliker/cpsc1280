#!/bin/sh

[ \
	"$(git config --get user.email)" = \
	'dev.nospam@jeremyh.com' \
] || return 0
# if are instructor
log DEBUG "Hello master!"

___1280_instructor_repo() (  # subshell
	local readonly repoGlob='*git???.com[:/]jhilliker/cpsc1280.git'

	# check that we have the 1280 repo & cd to it
	[ -n "$CPSC1280_DIR" ] \
	|| fatal "\$CPSC1280_DIR not set"
	cd "$CPSC1280_DIR" \
	|| fatal "couldnt cd to \$CPSC1280_DIR=$CPSC1280_DIR"
	[ -d '.git' ] \
	|| fatal "not a git repo: \$CPSC1280_DIR=$CPSC1280_DIR"

	local originURL="$(git remote get-url origin)"
	[ -n "$originURL" ] \
	&& [ -z "${originURL##$repoGlob}" ] \
	|| fatal "not the cpsc1280 repo: \$CPSC1280_DIR=$CPSC1280_DIR"

	[ -w "." ] || chmod -R u+w . || fatal "chmod -R failed"

	log DEBUG "origin is: '$originURL'"

	if [ -z "${originURL##*http*}" ] ; then  # contains

		log DEBUG 'checking SSH key'
		local ssh= #seperate lines so $? works
		ssh="$(ssh -T git@gitlab.com 2>&1)" \
		&& [ -z "${ssh##*Welcome to GitLab*}" ] \
		|| {
			log ERR "SSH key not set. Got message:$nl$ssh"
			fatal "Set at:${nl}https://gitlab.com/profile/keys"
		}

		# need to reset remote to ssh url
		local ssh="$(
			printf -- '%s\n' "$originURL" \
			| sed -E \
				-e 's|^https?://|git@|' \
				-e 's|/|:|'  # only the first one
		)"
		log INFO "Switching http repo to ssh: '$ssh'"
		git remote rename origin http
		git remote add origin "$ssh"

		originURL="$(git remote get-url origin)"
		log DEBUG "origin is now: '$originURL'"

		git fetch origin --unshallow \
		|| git fetch origin

		git branch -u origin/master
	fi
)
___1280_instructor_repo \
&& unset ___1280_instructor_repo
