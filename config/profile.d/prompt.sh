
[ -n "$PS1" ] || return 0

unset PS1_PREFIX PS1_SUFFIX

if [ -n "$COLORTERM" ] ; then
	[ -n "$LS_COLORS_DIR" ] || LS_COLORS_DIR="${STYLE_BOLD}${FG_BLUE}" # overridden in environment.sh
	PS1_SUFFIX="\[\${LS_COLORS_DIR}\]\w\[${RST}\] \\\$ "
else
	PS1_SUFFIX='\w \$ '
fi

# set the prompt
PS1="$PS1_PREFIX$PS1_SUFFIX"
