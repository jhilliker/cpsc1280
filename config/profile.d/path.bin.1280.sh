
___1280_add_bin_to_path() {
	local binDir="$CPSC1280_DIR/bin"
	[ -d "$binDir" ] || return 1

	PATH="$binDir:$PATH"
}
___1280_add_bin_to_path \
&& unset ___1280_add_bin_to_path
