
isdefined git || return 0

# https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh
___prompt_git_set() {
	local possibleLocations='
		/usr/lib/git-core/git-sh-prompt  '`#ubuntu`'
		/usr/share/git-core/contrib/completion/git-prompt.sh
		/usr/share/git/git-prompt.sh
		/usr/share/git/completion/git-prompt.sh
		/etc/profile.d/git-prompt.sh
	'
	local f=
	for f in $possibleLocations ; do
		! isdefined __git_ps1 || return 0	# only load one of them
		[ -f "$f" ] || continue
		. "$f" || log ERR "sourcing: '$f'"
	done
	isdefined __git_ps1
}

if ___prompt_git_set ; then
	if [ "$USERDNSDOMAIN" != 'INT.AD.LANGARA.CA' ] ; then
		GIT_PS1_SHOWDIRTYSTATE='t'
		GIT_PS1_SHOWSTASHSTATE='t'
		GIT_PS1_SHOWUNTRACKEDFILES='t'
		GIT_PS1_SHOWUPSTREAM='auto'
	fi
	#GIT_PS1_DESCRIBE_STYLE='branch'
	GIT_PS1_SHOWCOLORHINTS='auto'
	#GIT_PS1_HIDE_IF_PWD_IGNORED='t'
	PROMPT_COMMAND="$PROMPT_COMMAND
		__git_ps1 \"$PS1_PREFIX\" \"$PS1_SUFFIX\" '(%s)'
	"
else
	log WARN "Couldn't find '__git_ps1'"
fi

unset ___prompt_git_set
