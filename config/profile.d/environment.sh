
##### env variables ###################
export TZ='America/Vancouver'

export VISUAL='nano'
export EDITOR="$VISUAL"

export PAGER='less'
export LESS='-KMRSx4~#4'
# F quit if fits on one screen
# K quit on SIGINT
# M long prompt
# N number each line
# R output raw colors
# S chop long lines
# x tab width
# X don't clear screen
# ~ lines after file are blank, not ~
# # horizontal scroll shift

# make less more friendly for non-text input files, see lesspipe(1)
isdefined lesspipe && eval "$(lesspipe)"

##### shell settings ##################

# don't exit on ^D
set -o ignoreeof

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check window size after each command
shopt -s checkwinsize

##### color stuff #####################

if [ -n "$COLORTERM" ] ; then

	# colors for less
	export LESS_TERMCAP_mb="$STYLE_BOLD$FG_RED"   # begin blink
	export LESS_TERMCAP_md="$STYLE_BOLD$FG_GREEN" # begin bold
	export LESS_TERMCAP_me="$RST"                 # reset bold/blink
	export LESS_TERMCAP_so="$BG_BLUE$STYLE_BOLD$FG_WHITE" # begin reverse video
	export LESS_TERMCAP_se="$RST"                  # reset reverse video
	export LESS_TERMCAP_us="$STYLE_BOLD$FG_YELLOW" # begin underline
	export LESS_TERMCAP_ue="$RST"                  # reset underline

	#colors for gcc
	export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

	# colors for ls
	if isdefined dircolors ; then
		eval "$(dircolors -b)"
		LS_COLORS_DIR="$(
			dircolors -p | sed -En '/^DIR\s/ {
				s/DIR\s+(\S+)\s+.*/\1/
				p
				q
			}'
		)"
		LS_COLORS_DIR=$'\033['"${LS_COLORS_DIR}m"
	fi

fi
