
___rvm_path() {
	local rvmPath="$HOME/.rvm/bin"
	if [ -z "${PATH##*:$rvmPath*}" ] ; then
		# rvm must be last because ruby devs are snowflakes
		# oh, and it's added a ton of times...
		PATH="$(sed "s|:$rvmPath||g" <<< "$PATH"):$rvmPath"
	fi
}
___rvm_path \
&& unset ___rvm_path
