
___locale_prefs_LANG='
	en_CA.utf8
	en_US.utf8
	en_GB.utf8
	C.utf8
	C.UTF-8
	C
	POSIX
'

___locale_prefs_LC_COLLATE="
	C
	POSIX
	C.utf8
	C.UTF-8
	$___locale_prefs_LANG
"

___locale_available="$(locale -a)"

___locale_findbest() {
	printf -- '%s\n' "$@" \
	| grep -Fx -m1 "$___locale_available"
}

___locale_set() {
	local status=0
	local src=
	local var=
	local loc=
	for src in $(set | grep -o '^___locale_prefs_\w*=') ; do
		src="${src%%=*}"
		var="${src#___locale_prefs_}"  # global variable name
		eval src="\"\$$src\""          # list of locales
		loc="$(___locale_findbest $src)"
		if [ -n "$loc" ] ; then
			eval export "$var"="$loc"
		else
			log WARN "Couldn't find locale for '$var'"
			status=1
		fi
		log DEBUG "$(locale | grep --color=never "^$var=")"
	done

	return $status
}
___locale_set \
&& unset $(set | grep -o '^___locale_\w*')
