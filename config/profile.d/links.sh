#!/bin/sh

___1280_makeLinks() {
	# cant contain spaces or :
	local toLinks="
		$HOME/bin:$HOME/environment/bin
		$CPSC1280_DIR/data:$HOME/environment/1280data
		$CPSC1280_DIR/bin:$HOME/environment/1280bin
	"

	local mapping src dst
	for mapping in $toLinks ; do
		src="${mapping%%:*}"  # LHS
		dst="${mapping##*:}"  # RHS
		[ -e "$src" ] && [ ! -e "$dst" ] || continue
		(
			dstDir="${dst%/*}"
			dst="${dst##*/}"
			src="$(realpath -e --relative-base "$HOME" --relative-to "$dstDir" "$src")"
			cd "$dstDir" \
			&& rm -f "$dst" \
			&& ln -sT "$src" "$dst"
		) || log ERR "linking '$src' '$dst'"
	done
}
___1280_makeLinks \
&& unset ___1280_makeLinks
