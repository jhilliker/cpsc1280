
___1280_install_packages() {
	local numRetries=2
	local sleepLen=2
	local pkgs='
		bc
		dos2unix
		cowsay
		zstd
	'
	local installPackages=
	local pkg=
	for pkg in $pkgs ; do
		isdefined "$pkg" || {
			log DEBUG "Missign package: '$pkg'"
			installPackages="$installPackages $pkg"
		}
	done
	[ -n "$installPackages" ] || return 0

	# if any are missing, install all of them
	local retry=0
	while : ; do
		log INFO "Installing packages: $pkgs"
		if : &&
			sudo apt-get -y update &&
			sudo apt-get -y install $pkgs
		then
			log INFO "Finished installs."
			return 0
		elif [ -z "$numRetries" ] || [ $retry -ge $numRetries ] ; then
			log ERROR "Failed to install packages. Try again later."
			return 1
		fi
		: $(( retry += 1 ))
		log INFO "Trying again... Retry #$retry of $numRetries..."
		sleep $sleepLen
	done
}
___1280_install_packages \
&& unset ___1280_install_packages
