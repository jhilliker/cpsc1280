#!/bin/sh

___ssh_keys_check() {

	local ___ssh_key_pub="$HOME/.ssh/id_ed25519.pub"

	if ! [ -f "$___ssh_key_pub" ] ; then

		local ___ssh_key_comment=
		if [ -n "$C9_PROJECT" ] ; then
			___ssh_key_comment="c9:$C9_PROJECT"
		else
			___ssh_key_comment="$(hostname)"
		fi
		___ssh_key_comment="$___ssh_key_comment:$(date -I)"

		log INFO "Generating ed25519 keypair (no passphrase)"
		if ssh-keygen -q -t ed25519 -o \
			-N '' -f "${___ssh_key_pub%.pub}" -C "$___ssh_key_comment"
		then
			log DEBUG "Set passphrase with:
				ssh-keygen -c -f '${___ssh_key_pub%.pub}' -P '<passphrase>'"
		else
			log ERR 'Failed making keypair'
		fi
	fi
	! [ -e "$___ssh_key_pub" ] \
	|| log DEBUG "ssh pub-key:$nl$(cat "${___ssh_key_pub}")"
}
___ssh_keys_check \
&& unset ___ssh_keys_check
