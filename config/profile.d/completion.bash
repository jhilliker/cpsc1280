
___bash_completion() {
	local possibleLocations='
		/etc/bash_completion                        '`#ubuntu`'
		/usr/share/bash-completion/bash_completion  '`#arch`'
	'
	local f
	for f in $possibleLocations ; do
		[ -f "$f" ] || continue
		. "$f" \
			&& return 0 \
			|| log ERR "sourcing: '$f'"
	done
	return 1

}
___bash_completion \
&& unset ___bash_completion
