
___1280_fixGitconfig() {
	# aws c9 keeps setting core.editor incorrectly
	# and creating repeating [core] sections
	local gitconfig="$(
		< ~/.gitconfig \
		sed '/^\s*editor\s*=/ d' \
		| uniq \
		| sed \
			-e '$ s/^\[.*//' \
			-e '/^$/ d'
	)"
	printf -- '%s\n' "$gitconfig" > ~/.gitconfig
}
___1280_fixGitconfig \
&& unset ___1280_fixGitconfig

___1280_checkGitNameEmail() {
	local keys='user.name user.email'
	local key val
	for key in $keys ; do
		val="$(git config --global --get "$key")"
		if [ -n "$val" ] ; then
			log DEBUG "gitconfig: $key = $val"
		else
			log WARN "gitconfig: '$key' is unset. Set with: git config --global '$key' 'value'"
		fi
	done
}
___1280_checkGitNameEmail \
&& unset ___1280_checkGitNameEmail

___1280_git_completion_isSet() {
	complete -p | grep -q 'git'
}
___1280_git_completion() {
	# https://raw.githubusercontent.com/git/git/master/contrib/completion/git-completion.bash
	# default loaded from: /usr/share/bash-completion/completions/git
	local local possibleLocations='
		/usr/share/git-core/contrib/completion/git-completion.bash
		/etc/bash_completion.d/git
	'
	local f
	for f in $possibleLocations ; do
		! ___1280_git_completion_isSet || break  # only load one of them
		[ -f "$f" ] || continue
		. "$f" || log ERR "sourcing: '$f'"
	done
}
___1280_git_completion \
&& unset ___1280_git_completion ___1280_git_completion_isSet
