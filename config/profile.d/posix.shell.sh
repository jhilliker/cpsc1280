log DEBUG "sh -> $(which sh) -> $(readlink -e $(which sh))"

___check_is_posix_shell() {
	local badSHscript='
		#!/bin/sh
		tmp=(not allowed in POSIX) # no arrays allowed
		exit 0
	'
	printf -- '%s\n' "$badSHscript" |
	sh  >/dev/null 2>&1 &&
	log ERROR "'sh' allowed arrays! 'sh' is not POSIX." ||  # expecting failure
	log DEBUG "'sh' did not allow arrays. 'sh' is POSIX."
}
___check_is_posix_shell && unset ___check_is_posix_shell
