
# Uses age of last fetch.
___1280_git_pull_timed() {
	local recheckHours=8
	1280coursepull -W $(( recheckHours * 60 * 60 ))

	case $? in
		0) return 0 ;;
		42)
			log INFO '***** Restarting Bash *****'
			exec bash -i ;;
		*) return 1 ;;
	esac
}
___1280_git_pull_timed \
&& unset ___1280_git_pull_timed
