isdefined nvm \
|| { [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" ; } \
|| {
	log ERR "nvm not installed"
	return 1
}

___1280_node_14() {
	printf '%s\n' "$(nvm current)" v14 | sort -V -C || return 0

	printf '%s\n' 0.37 "$(nvm --version)" | sort -V -C \
	|| curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash \
	&& . "$NVM_DIR/nvm.sh" \
	|| return 1

	nvm install --default 14 \
	&& nvm uninstall 10
}
___1280_node_14
unset ___1280_node_14

log DEBUG "$(nvm current)"
