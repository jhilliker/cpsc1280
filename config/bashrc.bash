#!/bin/bash
#      ^^^^ uses "${BASH_SOURCE[0]}"

[ -n "$PS1" ] || return 0  # ensure we're interactive

readonly nl='
'

___1280_bashrc_load() {
	local readonly real0="$(realpath -e "${BASH_SOURCE[0]}")"
	local readonly scriptDir="${real0%/*}"
	export CPSC1280_DIR="$(realpath -e "$scriptDir/..")"
	local libDir="$CPSC1280_DIR/config/profile.d"

	local srcFirst="
		$libDir/path.bin.1280.sh
		$libDir/console.log.sh
		$libDir/color.sh
		$libDir/environment.sh
		$libDir/1280.course.pull.sh
		$libDir/locale.sh
	"

	isdefined() {
		type "$1" >/dev/null 2>&1
	}

	### basic logging, replaced later
	log() {
		printf -- '*** %s\n' "$*"
	} >&2
	fatal() {
		log FATAL "$@"
		exit 1
	}

	### src the files
	local alreadySourced=' '
	sourceMany() {
		local f
		for f ; do
			[ -n "${alreadySourced%%*$f*}" ] || continue
			. "$f" || log ERR "Error sourcing '$f'"
			alreadySourced="$alreadySourced $f"
		done
	}
	sourceMany $srcFirst "$libDir"/*  # the pull will restart shell if files changed

	unset isdefined \
		___log_getLogLevel_info ___log_getLogLevel_info_ log fatal \
		LOG_LEVEL
}
___1280_bashrc_load
