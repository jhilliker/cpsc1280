#!/bin/sh

# curl -L https://bit.ly/39QozHZ | sh

readonly dstDir="$HOME/environment/.cpsc1280"

readonly gitProjectHttpsUrl='https://gitlab.com/jhilliker/cpsc1280.git'
readonly setupSrc='install/deploy.sh'

readonly cloneDepth=2

type log >/dev/null 2>&1 || log() {
	printf -- '*** %s\n' "$*"
} >&2
type fatal >/dev/null 2>&1 || fatal() {
	log FATAL "$@"
	exit 1
}

log DEBUG "\$C9_PROJECT=$C9_PROJECT"
[ -n "$C9_PROJECT" ] \
|| fatal '
	Cowardly refusing to run.
	This should only be run on cloud9.
'

if [ -e "$dstDir" ] ; then
	log INFO "Removing '$dstDir'"
	sudo rm -rf "$dstDir" \
	|| fatal
fi

git clone --depth $cloneDepth "$gitProjectHttpsUrl" "$dstDir" \
&& chmod -R a-w "$dstDir" \
&& chmod -R u+w "$dstDir/.git" \
&& cd "$dstDir/${setupSrc%/*}" \
&& exec "./${setupSrc##*/}"

fatal # uneachable in happy path
