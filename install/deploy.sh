#!/bin/sh

configDir='config'
readonly bashrcFile='bashrc.bash'
readonly toSrc='
	profile.d/color.sh
	profile.d/console.log.sh
'

readonly toDeletes="
	$HOME/environment/README.md:47c4166af0e4e93fb0d2a0c0a5d122748a6a3ad5
"

#######################################

readonly vMplatformMsg='
	Please create a new VM using:

	Step 2:
	Platform
	"Ubuntu Server 18.04 LTS"

	see:  https://i.imgur.com/Ty4MdD4.png
'
readonly badSHscript='
#!/bin/sh
tmp=(not allowed in POSIX) # no arrays allowed
exit 0
'

readonly tag='### cpsc1280'

###############
##### logging
type log >/dev/null 2>&1 || log() {
	printf -- '*** %s\n' "$*"
} >&2
type fatal >/dev/null 2>&1 || fatal() {
	log FATAL "$@"
	exit 1
}

#######################################
##### init
#######################################

readonly real0="$(realpath -e "$0")"
readonly scriptDir="${real0%/*}"
readonly projectDir="${scriptDir%/install}"
# sanity check
[ "$projectDir/install/deploy.sh" -ef "$0" ] \
|| fatal "Invalid \$projectDir=$projectDir"

cd "$projectDir/$configDir" || fatal
readonly configDir="$PWD"
log DEBUG "\$configDir=$configDir"

for src in $toSrc ; do
	. "$src" || fatal "error sourcing script"
done

#######################################
##### sanity & env checks
#######################################
log DEBUG 'running sanity checks'

log DEBUG 'on cloud9'
[ -n "$C9_PROJECT" ] \
|| fatal '
	Cowardly refusing to run.
	This should only be run on cloud9.
'

log DEBUG 'on Ubuntu'
eval "$(cat /etc/*-release | grep -m1 '^NAME=".*"$')"
[ "$NAME" = 'Ubuntu' ] || {
	log ERR "NAME='$NAME'"
	uname -a
	fatal "'$NAME' is not POSIX compliant. $vMplatformMsg"
}

log DEBUG "'sh' is POSIX. No arrays"
printf -- '%s\n' "$badSHscript" \
| sh  >/dev/null 2>&1 \
&& fatal "sh is not POSIX. $vMplatformMsg"  # expecting failure

log INFO 'Passed sanity checks'

#######################################
##### ~/.bashrc
#######################################
log DEBUG "Preparing '~/.bashrc'"

src="$configDir/$bashrcFile" \
&& [ -f "$src" ] \
|| fatal "'$src' does not exist"

src='[ -z "$PS1" ] || '". '$src' $tag"

log DEBUG "adding to ~/.bashrc: '$src'"
readonly newBashrc="$(
	printf -- '%s\n\n%s\n' \
		"$(sed -e "/$tag/d" ~/.bashrc)" `# strip trailing newlines` \
		"$src"
)" \
&& [ -n "$newBashrc" ] \
&& printf -- '%s\n' "$newBashrc" > ~/.bashrc \
|| fatal 'making ~/.bashrc'

#######################################
##### gitconfig
#######################################
log DEBUG "gitconfig: including '$configDir/gitconfig'"
git config --global include.path "$configDir/gitconfig" \
|| fatal "Error including '$configDir/gitconfig' in git config"

#######################################
##### toDelete
#######################################
for mapping in $toDeletes ; do
	src="${mapping%%:*}"
	sha="${mapping##*:}"
	if [ -f "$src" ] && printf '%s  %s\n' "$sha" "$src" | shasum -sc - ; then
		log DEBUG "removing '$src'"
		rm -f "$src" || log ERR "removing '$src'"
	fi
done

#######################################
##### done
#######################################
log 'Done'
log INFO 'Close this terminal and open a new one for all changes to take effect.'
exit 0  # fatal exit above on err
