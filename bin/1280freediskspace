#!/bin/bash
#      ^^^^ just for line # on logs

##### settings ########################

readonly warnMegs=200

##### deps ############################

type log >/dev/null 2>&1 || log() {
	printf -- '*** %s\n' "$*"
} >&2
type fatal >/dev/null 2>&1 || fatal() {
	log FATAL "$@"
	exit 1
}

readonly real0="$(realpath -e "$0")"
readonly scriptDir="${real0%/*}"
readonly deployDir=$(realpath -e "$scriptDir/..")
readonly libDir="$deployDir/config/profile.d"

. "$libDir/console.log.sh" || fatal
. "$libDir/color.sh" || fatal

# sets systemd/journad max file usage
setSystemdJournal() {
	local path='/etc/systemd/journald.conf.d/90-journal-size.conf'
	local content="$(
		printf '[Journal]\nSystemMaxUse=%uM' 20
	)"
	if ! { echo "$content" | cmp -s "$path" - 2>/dev/null ; } ; then
		log DEBUG "Setting '$path':"
		sudo mkdir -p "${path%/*}" &&
		echo "$content" | sudo tee "$path" &&
		sudo systemctl restart systemd-journald.service
	fi
}

##### program #########################

## sudo
setSystemdJournal
sudo apt-get -y clean

## user
rm -rf ~/.cache/Homebrew ~/.cache/pip
[ ! -d ~/.npm/_logs/ ] ||
	find ~/.npm/_logs/ -type f -mtime +3 -delete # logs older than 3 days

## display
log DEBUG "
$(df -h ~ | sed 's/^/\t/')"

availMegs="$(df ~ --output=avail -BM | tr -dc '[:digit:]')"
[ "$availMegs" -gt "$warnMegs" ] || {
	log WARN  "only $availMegs MB free"
	log DEBUG "searching for 'node_modules' ..."
	node_modules="$(
		cd ~ &&
		find . -name node_modules -prune -print0 |
		xargs -r0 du -sh |
		sed 's,\./,~/,'
	)"
	if [ -n "$node_modules" ] ; then
		log INFO "$nl$node_modules"
	else
		log DEBUG '...none found'
	fi
}
